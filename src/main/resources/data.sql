INSERT INTO BOOKS(title, author, isbn) VALUES
('TestBook1', 'TestAuthor1', '123abc'),
('TestBook2', 'TestAuthor2', '456def');

INSERT INTO COMMENTS(content, bookId) VALUES
('comment1', 1),
('comment2', 1);
