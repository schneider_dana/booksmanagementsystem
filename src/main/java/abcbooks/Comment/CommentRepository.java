package abcbooks.Comment;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {

    @Query(value = "SELECT * FROM COMMENTS WHERE bookId = :bookId", nativeQuery = true)
    List<Comment> findByBookId(@Param("bookId") Integer bookId);
}
