package abcbooks.Comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    CommentRepository commentRepository;

    public List<Comment> getAllCommentsByBookId(int id) {
        return commentRepository.findByBookId(id);
    }

    public void save(Comment comment) {
        commentRepository.save(comment);
    }
}
