package abcbooks.Comment;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Entity
@Table(name = "COMMENTS")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String content;
    @Column(name = "bookId")
    private int bookId;
    private Date date;

    public Comment() {
        this.date = new Date();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public void setDate(java.sql.Date date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public int getBookId() {
        return bookId;
    }

    public LocalDate getDate() {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
