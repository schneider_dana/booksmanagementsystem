package abcbooks.Book;

public class PresentableBook {

    private Book book;
    private int commentCount;

    public PresentableBook(Book book, int commentCount) {
        this.book = book;
        this.commentCount = commentCount;
    }

    public int getId() {
        return book.getId();
    }

    public String getTitle() {
        return book.getTitle();
    }

    public String getAuthor() {
        return book.getAuthor();
    }

    public String getIsbn() {
        return book.getIsbn();
    }

    public int getCommentCount() {
        return commentCount;
    }
}
