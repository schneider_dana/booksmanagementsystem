package abcbooks.Controllers;

import abcbooks.Book.Book;
import abcbooks.Book.BookService;
import abcbooks.Book.PresentableBook;
import abcbooks.Comment.Comment;
import abcbooks.Comment.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private CommentService commentService;

    @GetMapping("/")
    public String displayBooks(Model model) {
        List<Book> books = bookService.getAllBooks();
        List<PresentableBook> presentableBooks = new ArrayList<>();

        for (Book book : books) {
            int commentCount = commentService.getAllCommentsByBookId(book.getId()).size();
            presentableBooks.add(new PresentableBook(book, commentCount));
        }

        model.addAttribute("books", presentableBooks);
        return "index";
    }

    @GetMapping("/addBook")
    public String addBookForm(Model model) {
        model.addAttribute("book", new Book());
        return "addBook";
    }

    @PostMapping("/saveBook")
    public RedirectView bookSubmit(@ModelAttribute Book book) {
        bookService.saveOrUpdate(book);
        return new RedirectView("/");
    }

    @GetMapping("/book/{id}")
    public String displayBookAndComments(@PathVariable("id") int id, Model model) {
        model.addAttribute("book", bookService.getBookById(id));
        model.addAttribute("comments", commentService.getAllCommentsByBookId(id));

        Comment newComment = new Comment();
        newComment.setBookId(id);
        model.addAttribute("comment", newComment);

        return "book";
    }

    @PostMapping("/saveComment")
    public RedirectView commentSubmit(@ModelAttribute Comment comment) {
        commentService.save(comment);
        return new RedirectView("/book/" + comment.getBookId());
    }
}
