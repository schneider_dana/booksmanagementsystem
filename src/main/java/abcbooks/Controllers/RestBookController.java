package abcbooks.Controllers;

import abcbooks.Book.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestBookController {

    @Autowired
    private BookService bookService;

    @DeleteMapping("/book/delete/{id}")
    public int bookDelete(@PathVariable("id") int id) {
        bookService.delete(id);
        return id;
    }
}
